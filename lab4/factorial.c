#include <stdio.h>
#include <limits.h>
#include <math.h>

long maxlong(void) {
	return LONG_MAX;
}

double upper_bound(long n) {
	if (n <6) {
		return 100;
	}
	else {
		double x = pow((n/2.),n);
		return x;
	}	 
}

double factorial(long num) {
	int a;
	double fact=1.;
	for (a = 1;a<=num;a++) {
		fact *=a;
	}
	return fact;
	
}


int main(void) {
    long i;

    /* The next line should compile once "maxlong" is defined. */
    /*printf("maxlong()=%1d\n", maxlong());*/

    /* The next code block should compile once "upper_bound" is defined. */

    
    for (i=0; i<10; i++) {
        printf("upper_bound(%ld)=%g\n", i, upper_bound(i));
    }
    
	printf("%g",factorial(4));

	getchar();
    return 0;
}

