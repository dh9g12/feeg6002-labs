#include <stdio.h>
#include <math.h>
#define XMIN 1.0
#define XMAX 10.0
#define N 10.0

int main(void) {
	int i;
	float x;
	for(i=0;i<N;i++) {
		x = XMIN + ((XMAX-XMIN)/(N-1))*i;
		printf ("%f %f\n",x,sin(x));
	}
	return 0;
}


